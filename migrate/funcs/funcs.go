package funcs

import (
	"awesomeProject2/db"
	"database/sql"
)
type Fields struct {
	X string
	Y int
}

func Run(db *sql.DB) error {
	f := []Fields{
		{
			X:"str",
			Y:122,
		},
		{
			X:"string",
			Y:344,
		},
		{
			X:"stroka",
			Y:756,
		},
	}
	var err error
	for _, fields := range f {
		err = database.Insert(db, fields.X, fields.Y)
		if err != nil {
			return err
		}
	}
	return nil
}

func MigrateUp(db *sql.DB) error {
	return database.Create(db)
}

func MigrateDown(db *sql.DB) error {
	return database.Drop(db)
}