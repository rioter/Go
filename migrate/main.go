package main

import (
	"awesomeProject2/db"
	"awesomeProject2/funcs"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

func main() {
	db, err := database.Connect()
	if err != nil {
		fmt.Println(err.Error())
	}
	app := cli.NewApp()
	app.Commands = cli.Commands{
		{
			Name: "migrate",
			Subcommands: cli.Commands{
				{
					Name: "down",
					Action: func(c *cli.Context) error {
						return funcs.MigrateDown(db)
					},
				},
				{
					Name: "up",
					Action: func(c *cli.Context) error {
						err = funcs.MigrateUp(db)
						return err
					},
				},
			},
		},
		{
			Name:  "run",
			Action: func(c *cli.Context) error {
					err = funcs.Run(db)
				return err
			},
		},
	}
	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Close()
	if err != nil {
		log.Fatal(err)
	}
}
