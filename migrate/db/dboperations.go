package database

import (
	"database/sql"
	"fmt"
)

func Connect() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:new-password@tcp(127.0.0.1:3306)/testdb")
	if err != nil {
		fmt.Println(err.Error())
	}
	return db,err
}
func Drop(db *sql.DB) error {
	_, err := db.Exec("DROP TABLE randtab")
	return err
}
func Create(db *sql.DB) error {
	_, err := db.Exec("CREATE Table randtab(id int NOT NULL AUTO_INCREMENT, str varchar(50), num int, PRIMARY KEY (id));")
	return err
}



func Insert(db *sql.DB, val1 string, val2 int) error {
	_, err := db.Exec("INSERT INTO randtab (str,num) VALUES (?,?)", val1, val2)
	return err
}
