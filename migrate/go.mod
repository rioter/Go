module awesomeProject2

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/urfave/cli v1.22.4
	github.com/urfave/cli/v2 v2.2.0
)
